# LaTeX2HTML 2020.2 (Released July 1, 2020)
# Associate images original text with physical files.


$key = q/-ZMAX<z<ZMAX;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.82ex; vertical-align: -0.31ex; " SRC="|."$dir".q|img74.png"
 ALT="$-ZMAX &lt; z &lt; ZMAX$">|; 

$key = q/0lealphale1;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.89ex; vertical-align: -0.43ex; " SRC="|."$dir".q|img56.png"
 ALT="$0 \le \alpha \le 1$">|; 

$key = q/10^{-5}cm^2slashs;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.59ex; vertical-align: -0.70ex; " SRC="|."$dir".q|img89.png"
 ALT="$10^{-5}cm^2/s$">|; 

$key = q/10^{12}mslashs^2;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.59ex; vertical-align: -0.70ex; " SRC="|."$dir".q|img48.png"
 ALT="$10^{12} m/s^2$">|; 

$key = q/180^o;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.68ex; vertical-align: -0.12ex; " SRC="|."$dir".q|img30.png"
 ALT="$180^o$">|; 

$key = q/<;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.40ex; vertical-align: -0.21ex; " SRC="|."$dir".q|img1.png"
 ALT="$&lt;$">|; 

$key = q/=A;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.68ex; vertical-align: -0.12ex; " SRC="|."$dir".q|img43.png"
 ALT="$=A$">|; 

$key = q/=omega;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.12ex; vertical-align: -0.12ex; " SRC="|."$dir".q|img45.png"
 ALT="$= \omega$">|; 

$key = q/>0;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.68ex; vertical-align: -0.21ex; " SRC="|."$dir".q|img55.png"
 ALT="$&gt; 0$">|; 

$key = q/>;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.40ex; vertical-align: -0.21ex; " SRC="|."$dir".q|img2.png"
 ALT="$&gt;$">|; 

$key = q/>=;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.40ex; vertical-align: -0.21ex; " SRC="|."$dir".q|img69.png"
 ALT="$&gt;=$">|; 

$key = q/A;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.68ex; vertical-align: -0.12ex; " SRC="|."$dir".q|img53.png"
 ALT="$A$">|; 

$key = q/AA;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.10ex; vertical-align: -0.12ex; " SRC="|."$dir".q|img70.png"
 ALT="$\AA$">|; 

$key = q/AA^{-1};MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.10ex; vertical-align: -0.12ex; " SRC="|."$dir".q|img24.png"
 ALT="$\AA^{-1}$">|; 

$key = q/B;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.61ex; vertical-align: -0.12ex; " SRC="|."$dir".q|img52.png"
 ALT="$B$">|; 

$key = q/Cos(theta);MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.38ex; vertical-align: -0.70ex; " SRC="|."$dir".q|img91.png"
 ALT="$Cos(\theta)$">|; 

$key = q/D;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.61ex; vertical-align: -0.12ex; " SRC="|."$dir".q|img21.png"
 ALT="$D$">|; 

$key = q/Delta;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.68ex; vertical-align: -0.12ex; " SRC="|."$dir".q|img31.png"
 ALT="$\Delta$">|; 

$key = q/H,C,N,O,P;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.17ex; vertical-align: -0.57ex; " SRC="|."$dir".q|img75.png"
 ALT="$H,C,N,O,P$">|; 

$key = q/Hz;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.68ex; vertical-align: -0.12ex; " SRC="|."$dir".q|img46.png"
 ALT="$Hz$">|; 

$key = q/K_1,...,K_5;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.10ex; vertical-align: -0.57ex; " SRC="|."$dir".q|img39.png"
 ALT="$K_1,...,K_5$">|; 

$key = q/K_1,K_2;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.10ex; vertical-align: -0.57ex; " SRC="|."$dir".q|img35.png"
 ALT="$K_1,K_2$">|; 

$key = q/K_3;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.03ex; vertical-align: -0.46ex; " SRC="|."$dir".q|img36.png"
 ALT="$K_3$">|; 

$key = q/K_phi;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.31ex; vertical-align: -0.78ex; " SRC="|."$dir".q|img71.png"
 ALT="$K_\phi$">|; 

$key = q/K_{phi};MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.31ex; vertical-align: -0.78ex; " SRC="|."$dir".q|img32.png"
 ALT="$K_{\phi}$">|; 

$key = q/L_2(x)=0.5(3cos^2x-1);MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.59ex; vertical-align: -0.70ex; " SRC="|."$dir".q|img106.png"
 ALT="$L_2(x) = 0.5(3\cos^2 x -1)$">|; 

$key = q/M;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.61ex; vertical-align: -0.12ex; " SRC="|."$dir".q|img33.png"
 ALT="$M$">|; 

$key = q/P(t);MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.38ex; vertical-align: -0.70ex; " SRC="|."$dir".q|img100.png"
 ALT="$P(t)$">|; 

$key = q/R_{cutoff};MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.31ex; vertical-align: -0.78ex; " SRC="|."$dir".q|img54.png"
 ALT="$R_{cutoff}$">|; 

$key = q/R_{hyd}+deltar;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.38ex; vertical-align: -0.78ex; " SRC="|."$dir".q|img99.png"
 ALT="$R_{hyd}+\delta r$">|; 

$key = q/R_{hyd}-deltar;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.38ex; vertical-align: -0.78ex; " SRC="|."$dir".q|img98.png"
 ALT="$R_{hyd}-\delta r$">|; 

$key = q/R_{hyd};MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.31ex; vertical-align: -0.78ex; " SRC="|."$dir".q|img96.png"
 ALT="$R_{hyd}$">|; 

$key = q/S;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.75ex; vertical-align: -0.12ex; " SRC="|."$dir".q|img76.png"
 ALT="$S$">|; 

$key = q/Vslashcm;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.38ex; vertical-align: -0.70ex; " SRC="|."$dir".q|img44.png"
 ALT="$V/cm$">|; 

$key = q/[-1,1];MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.38ex; vertical-align: -0.70ex; " SRC="|."$dir".q|img90.png"
 ALT="$[-1,1]$">|; 

$key = q/alpha;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.12ex; vertical-align: -0.12ex; " SRC="|."$dir".q|img50.png"
 ALT="$\alpha$">|; 

$key = q/alpha_m=(M-m)slash(M-1);MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.38ex; vertical-align: -0.70ex; " SRC="|."$dir".q|img62.png"
 ALT="$\alpha_m = (M-m)/(M-1)$">|; 

$key = q/alphaslashR_{cutoff};MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.45ex; vertical-align: -0.78ex; " SRC="|."$dir".q|img49.png"
 ALT="$\alpha /R_{cutoff}$">|; 

$key = q/b;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.68ex; vertical-align: -0.12ex; " SRC="|."$dir".q|img63.png"
 ALT="$b$">|; 

$key = q/b=3AA;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.17ex; vertical-align: -0.12ex; " SRC="|."$dir".q|img64.png"
 ALT="$b = 3 \AA$">|; 

$key = q/deltar;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.75ex; vertical-align: -0.12ex; " SRC="|."$dir".q|img102.png"
 ALT="$\delta r$">|; 

$key = q/displaystyle+left.frac{q_iq_j}{4piepsilon_0(r+b(1-alpha^{w2}))}right];MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.67ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img60.png"
 ALT="$\displaystyle + \left. \frac{q_i q_j}{4 \pi \epsilon_0 (r+b(1-\alpha^{w2}))} \right]$">|; 

$key = q/displaystyleC=langlefrac{3cos^2theta-1}{2|r|^3}rangle;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.73ex; vertical-align: -2.29ex; " SRC="|."$dir".q|img94.png"
 ALT="$\displaystyle C = \langle \frac{3\cos^2\theta - 1}{2\vert r\vert^3}\rangle$">|; 

$key = q/displaystyleD_{X}=frac{langleDeltax^2(t)rangle}{2t};MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.18ex; vertical-align: -1.71ex; " SRC="|."$dir".q|img88.png"
 ALT="$\displaystyle D_{X} = \frac{\langle \Delta x^2(t)\rangle}{2t}$">|; 

$key = q/displaystyleD_{av}=frac{langleDeltar^2(t)rangle}{6t};MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.18ex; vertical-align: -1.71ex; " SRC="|."$dir".q|img86.png"
 ALT="$\displaystyle D_{av} = \frac{\langle \Delta r^2(t)\rangle}{6t}$">|; 

$key = q/displaystyleD_{dif}=frac{1}{6}frac{partiallangleDeltar^2(t)rangle}{partialt};MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.18ex; vertical-align: -1.71ex; " SRC="|."$dir".q|img87.png"
 ALT="$\displaystyle D_{dif} = \frac{1}{6}\frac{\partial\langle \Delta r^2(t)\rangle}{\partial t}$">|; 

$key = q/displaystyleE(t)=Acos(2piomegat);MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.38ex; vertical-align: -0.70ex; " SRC="|."$dir".q|img42.png"
 ALT="$\displaystyle E(t) = A\cos(2\pi\omega t)
$">|; 

$key = q/displaystyleS=langlefrac{3cos^2theta-1}{2}rangle;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.11ex; vertical-align: -1.71ex; " SRC="|."$dir".q|img92.png"
 ALT="$\displaystyle S = \langle \frac{3\cos^2\theta - 1}{2}\rangle$">|; 

$key = q/displaystyleU=U_{LJ}+U_{el}+U_{bond}+U_{ang}+U_{tors}+U_{impr};MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.31ex; vertical-align: -0.78ex; " SRC="|."$dir".q|img3.png"
 ALT="$\displaystyle
U = U_{LJ} + U_{el} + U_{bond} + U_{ang} + U_{tors} + U_{impr}
$">|; 

$key = q/displaystyleU_{LJ}=sum_{non-bonded}4varepsilon_{ij}Big(big(frac{sigma_{ij}}{r_{ij}}big)^{12}-big(frac{sigma_{ij}}{r_{ij}}big)^6Big);MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.60ex; vertical-align: -3.34ex; " SRC="|."$dir".q|img4.png"
 ALT="$\displaystyle U_{LJ} = \sum_{non-bonded}4\varepsilon_{ij}\Big(\big(\frac{
\sigma_{ij}}{r_{ij}}\big)^{12}-\big(\frac{\sigma_{ij}}{r_{ij}}\big)^6\Big)
$">|; 

$key = q/displaystyleU_{ang}(theta)=k_{theta}(theta-theta_0)^2;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.80ex; vertical-align: -0.78ex; " SRC="|."$dir".q|img25.png"
 ALT="$\displaystyle
U_{ang}(\theta) = k_{\theta}(\theta - \theta_0)^2
$">|; 

$key = q/displaystyleU_{el}=sum_{non-bonded}frac{q_iq_j}{4piepsilon_0r_{ij}};MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.46ex; vertical-align: -3.34ex; " SRC="|."$dir".q|img13.png"
 ALT="$\displaystyle U_{el} = \sum_{non-bonded}\frac{q_iq_j}{4\pi\epsilon_0r_{ij}}
$">|; 

$key = q/displaystyleU_{harm}(r)=k(r-r_0)^2;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.73ex; vertical-align: -0.70ex; " SRC="|."$dir".q|img16.png"
 ALT="$\displaystyle
U_{harm}(r) = k(r - r_0)^2
$">|; 

$key = q/displaystyleU_{impr}(phi)=k_{phi}(phi-phi_0)^2;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.80ex; vertical-align: -0.78ex; " SRC="|."$dir".q|img40.png"
 ALT="$\displaystyle
U_{impr}(\phi) = k_{\phi}(\phi - \phi_0)^2
$">|; 

$key = q/displaystyleU_{morse}(r)=Dbig(1-exp(-rho(r-r_0))big)^2;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 3.36ex; vertical-align: -0.93ex; " SRC="|."$dir".q|img17.png"
 ALT="$\displaystyle
U_{morse}(r) = D\big(1-\exp(-\rho(r-r_0))\big)^2
$">|; 

$key = q/displaystyleU_{tors}(phi)=K_1(1+cos{phi})slash2+K_2(1-cos{2phi})slash2+K_3(1+cos{3phi})slash2;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.38ex; vertical-align: -0.70ex; " SRC="|."$dir".q|img34.png"
 ALT="$\displaystyle
U_{tors}(\phi) = K_1(1+\cos{\phi})/2 + K_2(1-\cos{2\phi})/2 + K_3(1+\cos{3\phi})/2
$">|; 

$key = q/displaystyleU_{tors}(phi)=K_{phi}(1+cos(Mphi-Delta));MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.45ex; vertical-align: -0.78ex; " SRC="|."$dir".q|img28.png"
 ALT="$\displaystyle
U_{tors}(\phi) = K_{\phi}(1+\cos(M\phi-\Delta))
$">|; 

$key = q/displaystyleU_{tors}(phi)=sum_{i=1}^5K_icos^i(phi-180);MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 6.85ex; vertical-align: -3.09ex; " SRC="|."$dir".q|img38.png"
 ALT="$\displaystyle
U_{tors}(\phi) = \sum_{i=1}^5 K_i\cos^i(\phi - 180)
$">|; 

$key = q/displaystyleV^{Ss}(alpha,r);MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.80ex; vertical-align: -0.70ex; " SRC="|."$dir".q|img57.png"
 ALT="$\displaystyle V^{Ss}(\alpha,r)$">|; 

$key = q/displaystylealpha^{w1}left[4varepsilonleft(left(frac{sigma}{r+b(1-alpha^{w2})}right)^{12}-left(frac{sigma}{r+b(1-alpha^{w2})}right)^6right)+right.;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 6.99ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img59.png"
 ALT="$\displaystyle \alpha^{w1} \left[ 4 \varepsilon
\left( \left(\frac{\sigma}{r+b(1...
...\right)^{12}-
\left(\frac{\sigma}{r+b(1-\alpha^{w2})}\right)^6\right) + \right.$">|; 

$key = q/displaystylec(t)=frac{langleL_2(vec{n}(t_0)cdotvec{n}(t_0+t))rangle}{langleL_2(vec{n}(t_0)^2)rangle};MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.53ex; vertical-align: -2.29ex; " SRC="|."$dir".q|img105.png"
 ALT="$\displaystyle
c(t) = \frac{\langle L_2(\vec{n}(t_0)\cdot\vec{n}(t_0+t))\rangle}
{\langle L_2(\vec{n}(t_0)^2)\rangle}
$">|; 

$key = q/displaystylec(t)=frac{langlevec{n}(t_0)cdotvec{n}(t_0+t)rangle}{langlevec{n}(t_0)^2rangle};MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 5.53ex; vertical-align: -2.29ex; " SRC="|."$dir".q|img104.png"
 ALT="$\displaystyle
c(t) = \frac{\langle\vec{n}(t_0)\cdot\vec{n}(t_0+t)\rangle}
{\langle\vec{n}(t_0)^2\rangle}
$">|; 

$key = q/displaystyleepsilon=1+frac{4pi}{3kTV}langle|sum_ivec{mu}_i|^2rangle;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 6.01ex; vertical-align: -3.09ex; " SRC="|."$dir".q|img77.png"
 ALT="$\displaystyle
\epsilon = 1 + \frac{4\pi}{3kTV} \langle \vert \sum_i \vec{\mu}_i \vert^2 \rangle
$">|; 

$key = q/displaystylelangleDeltar^2(t)rangle=langle(vec{r}(t_0+t)-vec{r}(t_0))^2rangle;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.73ex; vertical-align: -0.70ex; " SRC="|."$dir".q|img81.png"
 ALT="$\displaystyle
\langle \Delta r^2(t)\rangle = \langle (\vec{r}(t_0+t)-\vec{r}(t_0))^2\rangle
$">|; 

$key = q/erfc(A);MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.38ex; vertical-align: -0.70ex; " SRC="|."$dir".q|img51.png"
 ALT="$erfc(A)$">|; 

$key = q/fs;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.17ex; vertical-align: -0.57ex; " SRC="|."$dir".q|img72.png"
 ALT="$fs$">|; 

$key = q/g;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.54ex; vertical-align: -0.57ex; " SRC="|."$dir".q|img47.png"
 ALT="$g$">|; 

$key = q/gslashcm^3;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.59ex; vertical-align: -0.70ex; " SRC="|."$dir".q|img41.png"
 ALT="$g/cm^3$">|; 

$key = q/i,j;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.03ex; vertical-align: -0.57ex; " SRC="|."$dir".q|img10.png"
 ALT="$i,j$">|; 

$key = q/i;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.61ex; vertical-align: -0.12ex; " SRC="|."$dir".q|img6.png"
 ALT="$i$">|; 

$key = q/j;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.03ex; vertical-align: -0.57ex; " SRC="|."$dir".q|img7.png"
 ALT="$j$">|; 

$key = q/k;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.68ex; vertical-align: -0.12ex; " SRC="|."$dir".q|img19.png"
 ALT="$k$">|; 

$key = q/kJslashM;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.38ex; vertical-align: -0.70ex; " SRC="|."$dir".q|img37.png"
 ALT="$kJ/M$">|; 

$key = q/kJslashmol;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.38ex; vertical-align: -0.70ex; " SRC="|."$dir".q|img22.png"
 ALT="$kJ/mol$">|; 

$key = q/kJslashmolslashAA^2;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.73ex; vertical-align: -0.70ex; " SRC="|."$dir".q|img20.png"
 ALT="$kJ/mol/\AA^2$">|; 

$key = q/kT;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.68ex; vertical-align: -0.12ex; " SRC="|."$dir".q|img61.png"
 ALT="$kT$">|; 

$key = q/k_{theta};MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.03ex; vertical-align: -0.46ex; " SRC="|."$dir".q|img27.png"
 ALT="$k_{\theta}$">|; 

$key = q/phi;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.10ex; vertical-align: -0.57ex; " SRC="|."$dir".q|img29.png"
 ALT="$\phi$">|; 

$key = q/r_0;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.47ex; vertical-align: -0.46ex; " SRC="|."$dir".q|img18.png"
 ALT="$r_0$">|; 

$key = q/r_{ij};MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.75ex; vertical-align: -0.78ex; " SRC="|."$dir".q|img5.png"
 ALT="$r_{ij}$">|; 

$key = q/rho;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.61ex; vertical-align: -0.57ex; " SRC="|."$dir".q|img23.png"
 ALT="$\rho$">|; 

$key = q/s;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.12ex; vertical-align: -0.12ex; " SRC="|."$dir".q|img79.png"
 ALT="$s$">|; 

$key = q/sigma;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.12ex; vertical-align: -0.12ex; " SRC="|."$dir".q|img14.png"
 ALT="$\sigma$">|; 

$key = q/sigma_i;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.47ex; vertical-align: -0.46ex; " SRC="|."$dir".q|img12.png"
 ALT="$\sigma_i$">|; 

$key = q/sigma_{ij};MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.24ex; vertical-align: -0.78ex; " SRC="|."$dir".q|img9.png"
 ALT="$sigma_{ij}$">|; 

$key = q/t;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.54ex; vertical-align: -0.12ex; " SRC="|."$dir".q|img80.png"
 ALT="$t$">|; 

$key = q/t<t_{per};MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.17ex; vertical-align: -0.78ex; " SRC="|."$dir".q|img101.png"
 ALT="$t &lt; t_{per}$">|; 

$key = q/t_0;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.89ex; vertical-align: -0.46ex; " SRC="|."$dir".q|img82.png"
 ALT="$t_0$">|; 

$key = q/t_{beg};MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.17ex; vertical-align: -0.78ex; " SRC="|."$dir".q|img84.png"
 ALT="$t_{beg}$">|; 

$key = q/t_{beg}let_0let_{end}-t;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.17ex; vertical-align: -0.78ex; " SRC="|."$dir".q|img83.png"
 ALT="$t_{beg} \le t_0 \le t_{end}-t$">|; 

$key = q/t_{end};MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.89ex; vertical-align: -0.46ex; " SRC="|."$dir".q|img85.png"
 ALT="$t_{end}$">|; 

$key = q/t_{per};MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.17ex; vertical-align: -0.78ex; " SRC="|."$dir".q|img97.png"
 ALT="$t_{per}$">|; 

$key = q/textstyle=;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 0.63ex; vertical-align: -0.12ex; " SRC="|."$dir".q|img58.png"
 ALT="$\textstyle =$">|; 

$key = q/theta;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.75ex; vertical-align: -0.12ex; " SRC="|."$dir".q|img93.png"
 ALT="$\theta$">|; 

$key = q/theta_0;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.10ex; vertical-align: -0.46ex; " SRC="|."$dir".q|img26.png"
 ALT="$\theta_0$">|; 

$key = q/varepsilon;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.12ex; vertical-align: -0.12ex; " SRC="|."$dir".q|img15.png"
 ALT="$\varepsilon$">|; 

$key = q/varepsilon_i;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.47ex; vertical-align: -0.46ex; " SRC="|."$dir".q|img11.png"
 ALT="$\varepsilon_i$">|; 

$key = q/varepsilon_{ij};MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.75ex; vertical-align: -0.78ex; " SRC="|."$dir".q|img8.png"
 ALT="$\varepsilon_{ij}$">|; 

$key = q/vec{mu}_i;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.24ex; vertical-align: -0.57ex; " SRC="|."$dir".q|img78.png"
 ALT="$\vec{\mu}_i$">|; 

$key = q/vec{n}(t);MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.38ex; vertical-align: -0.70ex; " SRC="|."$dir".q|img103.png"
 ALT="$\vec{n}(t)$">|; 

$key = q/w1;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.61ex; vertical-align: -0.12ex; " SRC="|."$dir".q|img65.png"
 ALT="$w1$">|; 

$key = q/w1=1;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.61ex; vertical-align: -0.12ex; " SRC="|."$dir".q|img67.png"
 ALT="$w1 = 1$">|; 

$key = q/w2;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.61ex; vertical-align: -0.12ex; " SRC="|."$dir".q|img66.png"
 ALT="$w2$">|; 

$key = q/w2=1slash3;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.38ex; vertical-align: -0.70ex; " SRC="|."$dir".q|img68.png"
 ALT="$w2 = 1/3$">|; 

$key = q/z;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 1.12ex; vertical-align: -0.12ex; " SRC="|."$dir".q|img73.png"
 ALT="$z$">|; 

$key = q/|r|;MSF=1;AAT/;
$cached_env_img{$key} = q|<IMG STYLE="height: 2.38ex; vertical-align: -0.70ex; " SRC="|."$dir".q|img95.png"
 ALT="$\vert r\vert$">|; 

1;

