# LaTeX2HTML 2020.2 (Released July 1, 2020)
# Associate internals original text with physical files.


$key = q/FF/;
$ref_files{$key} = "$dir".q|node5.html|; 
$noresave{$key} = "$nosave";

$key = q/TCF/;
$ref_files{$key} = "$dir".q|node51.html|; 
$noresave{$key} = "$nosave";

$key = q/TCF-L2/;
$ref_files{$key} = "$dir".q|node51.html|; 
$noresave{$key} = "$nosave";

$key = q/diel/;
$ref_files{$key} = "$dir".q|node39.html|; 
$noresave{$key} = "$nosave";

$key = q/diffusion/;
$ref_files{$key} = "$dir".q|node40.html|; 
$noresave{$key} = "$nosave";

$key = q/harm/;
$ref_files{$key} = "$dir".q|node7.html|; 
$noresave{$key} = "$nosave";

$key = q/harm-ang/;
$ref_files{$key} = "$dir".q|node10.html|; 
$noresave{$key} = "$nosave";

$key = q/morse/;
$ref_files{$key} = "$dir".q|node7.html|; 
$noresave{$key} = "$nosave";

$key = q/softp/;
$ref_files{$key} = "$dir".q|node19.html|; 
$noresave{$key} = "$nosave";

$key = q/tors-0/;
$ref_files{$key} = "$dir".q|node9.html|; 
$noresave{$key} = "$nosave";

$key = q/tors1/;
$ref_files{$key} = "$dir".q|node10.html|; 
$noresave{$key} = "$nosave";

$key = q/tors5/;
$ref_files{$key} = "$dir".q|node10.html|; 
$noresave{$key} = "$nosave";

1;

