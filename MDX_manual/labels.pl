# LaTeX2HTML 2020.2 (Released July 1, 2020)
# Associate labels original text with physical files.


$key = q/FF/;
$external_labels{$key} = "$URL/" . q|node5.html|; 
$noresave{$key} = "$nosave";

$key = q/TCF/;
$external_labels{$key} = "$URL/" . q|node51.html|; 
$noresave{$key} = "$nosave";

$key = q/TCF-L2/;
$external_labels{$key} = "$URL/" . q|node51.html|; 
$noresave{$key} = "$nosave";

$key = q/diel/;
$external_labels{$key} = "$URL/" . q|node39.html|; 
$noresave{$key} = "$nosave";

$key = q/diffusion/;
$external_labels{$key} = "$URL/" . q|node40.html|; 
$noresave{$key} = "$nosave";

$key = q/harm/;
$external_labels{$key} = "$URL/" . q|node7.html|; 
$noresave{$key} = "$nosave";

$key = q/harm-ang/;
$external_labels{$key} = "$URL/" . q|node10.html|; 
$noresave{$key} = "$nosave";

$key = q/morse/;
$external_labels{$key} = "$URL/" . q|node7.html|; 
$noresave{$key} = "$nosave";

$key = q/softp/;
$external_labels{$key} = "$URL/" . q|node19.html|; 
$noresave{$key} = "$nosave";

$key = q/tors-0/;
$external_labels{$key} = "$URL/" . q|node9.html|; 
$noresave{$key} = "$nosave";

$key = q/tors1/;
$external_labels{$key} = "$URL/" . q|node10.html|; 
$noresave{$key} = "$nosave";

$key = q/tors5/;
$external_labels{$key} = "$URL/" . q|node10.html|; 
$noresave{$key} = "$nosave";

1;


# LaTeX2HTML 2020.2 (Released July 1, 2020)
# labels from external_latex_labels array.


$key = q/FF/;
$external_latex_labels{$key} = q|1|; 
$noresave{$key} = "$nosave";

$key = q/TCF/;
$external_latex_labels{$key} = q|15|; 
$noresave{$key} = "$nosave";

$key = q/TCF-L2/;
$external_latex_labels{$key} = q|16|; 
$noresave{$key} = "$nosave";

$key = q/diel/;
$external_latex_labels{$key} = q|13|; 
$noresave{$key} = "$nosave";

$key = q/diffusion/;
$external_latex_labels{$key} = q|14|; 
$noresave{$key} = "$nosave";

$key = q/harm/;
$external_latex_labels{$key} = q|4|; 
$noresave{$key} = "$nosave";

$key = q/harm-ang/;
$external_latex_labels{$key} = q|10|; 
$noresave{$key} = "$nosave";

$key = q/morse/;
$external_latex_labels{$key} = q|5|; 
$noresave{$key} = "$nosave";

$key = q/softp/;
$external_latex_labels{$key} = q|12|; 
$noresave{$key} = "$nosave";

$key = q/tors-0/;
$external_latex_labels{$key} = q|7|; 
$noresave{$key} = "$nosave";

$key = q/tors1/;
$external_latex_labels{$key} = q|8|; 
$noresave{$key} = "$nosave";

$key = q/tors5/;
$external_latex_labels{$key} = q|9|; 
$noresave{$key} = "$nosave";

1;

