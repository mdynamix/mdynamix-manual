# Documentation to **mdynamix**

Includes:
 - Latex source text
 - Printable .pdf
 - HTML format

## To use HTML documentation:

- download / clone the documentation folder

- type in the Web browser:

  file:///<download directory>/mdynamix-manual/MDX_manual/index.html

