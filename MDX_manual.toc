\contentsline {section}{\numberline {1}Introduction}{4}%
\contentsline {subsection}{\numberline {1.1}Changes from previous versions}{4}%
\contentsline {section}{\numberline {2}Description of the molecular structure and the force field}{6}%
\contentsline {subsection}{\numberline {2.1}The force field}{6}%
\contentsline {subsection}{\numberline {2.2}Molecular geometry and non-bonded interaction parameters}{7}%
\contentsline {subsection}{\numberline {2.3}Bonds}{7}%
\contentsline {subsection}{\numberline {2.4}Covalent angles}{8}%
\contentsline {subsection}{\numberline {2.5}Dihedral angles}{9}%
\contentsline {subsection}{\numberline {2.6}Optional features of the force field}{9}%
\contentsline {section}{\numberline {3}The main input file}{11}%
\contentsline {subsection}{\numberline {3.1}General notes}{11}%
\contentsline {subsection}{\numberline {3.2}Basic setup}{11}%
\contentsline {subsection}{\numberline {3.3}Restart control}{12}%
\contentsline {subsection}{\numberline {3.4}System}{13}%
\contentsline {subsection}{\numberline {3.5}Ensemble}{15}%
\contentsline {subsection}{\numberline {3.6}Molecular Dynamics}{17}%
\contentsline {subsection}{\numberline {3.7}Startup}{18}%
\contentsline {subsection}{\numberline {3.8}Expanded Ensemble mode}{20}%
\contentsline {subsection}{\numberline {3.9}Path Integral mode}{23}%
\contentsline {subsection}{\numberline {3.10}Properties}{23}%
\contentsline {section}{\numberline {4}Compilation}{25}%
\contentsline {subsection}{\numberline {4.1}Sequential execution}{25}%
\contentsline {subsection}{\numberline {4.2}Parallel execution}{25}%
\contentsline {subsection}{\numberline {4.3}Other makefiles}{26}%
\contentsline {section}{\numberline {5}Execution}{26}%
\contentsline {subsection}{\numberline {5.1}Files used by the program}{26}%
\contentsline {subsection}{\numberline {5.2}Memory considerations}{26}%
\contentsline {subsection}{\numberline {5.3}Program structure}{27}%
\contentsline {section}{\numberline {6}{\em Makemol} utility}{28}%
\contentsline {subsection}{\numberline {6.1}General organization}{28}%
\contentsline {subsection}{\numberline {6.2}The force field (.ff) file}{29}%
\contentsline {subsection}{\numberline {6.3}char2mdx}{30}%
\contentsline {subsection}{\numberline {6.4}The molecular structure (.smol) file}{31}%
\contentsline {section}{\numberline {7}{\em Tranal} utility}{31}%
\contentsline {subsection}{\numberline {7.1}General organization}{31}%
\contentsline {subsection}{\numberline {7.2}tranal\_base: reading trajectories}{33}%
\contentsline {subsection}{\numberline {7.3}Computation of the electron density and electrostatic potential in bilayers: bileldens.f}{35}%
\contentsline {subsection}{\numberline {7.4}Dielectric constant: diel.f}{36}%
\contentsline {subsection}{\numberline {7.5}Diffusion: diffus.f}{37}%
\contentsline {subsection}{\numberline {7.6}Dryrun: dryrun.f}{38}%
\contentsline {subsection}{\numberline {7.7}Hydrogen bonds: h-bonds.f}{38}%
\contentsline {subsection}{\numberline {7.8}Lateral diffusion: latdiff.f}{39}%
\contentsline {subsection}{\numberline {7.9}Order parameters: order.f}{40}%
\contentsline {subsection}{\numberline {7.10}Radial distribution functions: rdf.f}{41}%
\contentsline {subsection}{\numberline {7.11}Gyration radius and end-to-end distance: rgyr.f}{42}%
\contentsline {subsection}{\numberline {7.12}Spatial distribution functions: sdf.f}{43}%
\contentsline {subsection}{\numberline {7.13}Distribution of torsion angles: torsion.f}{45}%
\contentsline {subsection}{\numberline {7.14}Ramachandran plot for a pair of torsion angles: torsion2.f}{46}%
\contentsline {subsection}{\numberline {7.15}Residence time calculation: restime.f}{46}%
\contentsline {subsection}{\numberline {7.16}Time correlation functions: trtcf.f}{48}%
